import subprocess
from typing import Dict
from typing import Optional


class Arguments:
    DEFAULTS = {
        "admin": "admin",
        "domain": ".",
        "language": "fr",
        "is_public": "yes",
    }

    _main_domain = None

    def __init__(self, app_id: str, args: Dict[str, str]):
        self.app_id = app_id
        self._args = self.DEFAULTS.copy()
        self._args.update(args)

    def __getitem__(self, key: str) -> str:
        mkey = "_" + key
        if mkey in self.__class__.__dict__:
            return self.__class__.__dict__[mkey](self)
        elif key in self._args:
            return self._args[key]
        else:
            raise IndexError(f"Index {key} does not exist in {self.__class__.__name__}")

    def format(self) -> str:
        args = ("".join((key, "=", self[key])) for key in self._args.keys())
        return "&".join(args)

    @classmethod
    def get_main_domain(cls) -> str:
        if cls._main_domain:
            return cls._main_domain

        try:
            completed_process = subprocess.run(
                ["yunohost", "domain", "main-domain"], text=True, capture_output=True
            )
        except FileNotFoundError:
            pass
        else:
            if completed_process.returncode == 0:
                cls._main_domain = completed_process.stdout.split()[1]
                return cls._main_domain

        cls._main_domain = "MAIN_DOMAIN"
        return cls._main_domain

    def _domain(self) -> str:
        if "domain" in self._args:
            if self._args["domain"][-1] == ".":
                return (self._args["domain"] + self.get_main_domain()).strip(".")
            else:
                return self._args["domain"]
        else:
            return self.get_main_domain()

    def _path(self) -> Optional[str]:
        if "path" in self._args:
            if self._args["path"][0] == "/":
                return self._args["path"]
            else:
                return "/" + self._args["path"]
        else:
            return None
