# Recy'Clic

Distribution Yunohost avec des contenus pédagogique de Moocs Colibris pré-installés.

# Documentation

## Utilisation

Le script se lance en tant que root sur une instance YunoHost vierge.

Remplacer `<admin_username>` par l'utilisateur qui sera administrateur de toutes les applications installées.

```sh
sudo ./install.py --admin <admin_username>
```

Il est possible d'utiliser l'option `-d` pour lancer une simulation et vérifier que les pré-requis sont présents.

```sh
sudo ./install.sh -d --admin <admin_username>
```

## Configuration

La configuration se fait à l'aide du fichier `list.json`, ou d'un autre fichier au même format, via l'argument `-f` ou `--file`.

Un exemple de configuration utilisant toutes les valeurs est disponible dans `list.example.json`.

Ce fichier contient une liste d'applications et de leurs options d'installation.

```json
[
    <application1>,
    <application2>,
    <application...>
]
```

Chaque application contient les valeurs suivantes:

- **`application`** (obligatoire): Identifiant YunoHost de l'application, pour `example_ynh` c'est `example`
- `url`: Une URL vers un dépôt non officiel, ou une branche spécifique, par défaut c'est la dernière version officielle qui est utilisée
- **`arguments`** (obligatoire): Les options d'installation pour l'application, cf [Arguments](#arguments)
- `default`: booléen, si l'application doit être l'appli par défaut.
- `postInstallScript`: Un script à lancer après l'installation
- `extensions`: Une liste d'extensions à installer, cf [Extensions](#extensions)

### Arguments

Un groupe `"clé": "valeur"` qui correspond aux paramètres définis dans le fichier manifeste de l'application.

Les valeurs par défaut sont:
```json
{
    "admin": "admin",
    "domain": ".",
    "path": "/<application>",
    "language": "fr",
    "is_public": "yes"
}
```

Les paramètres suivants ont un comportement particulier:

#### `domain`

Si la valeur de `domain` se finit par un point (`.`), le nom de domaine par défaut de l'instance est ajouté comme base du nom de domaine.

En mode dryrun, le nom d'instance par défaut est remplacé par `MAIN_DOMAIN` si le script n'est pas exécuté en tant que `root`.

Exemples pour une instance située sur `example.ynh.fr`:
- `"domain": "app."` devient `"domain": "app.example.ynh.fr"`
- `"domain": "."` devient `"domain": "example.ynh.fr"`
- `"domain": "mail.example.com"` reste `"domain": "mail.example.com"`

#### `path`

Un `/` est ajouté au début s'il n'est pas déjà présent.
L'identifiant d'application est utilisé comme valeur par défaut.

Exemples pour l'application `example_ynh`:

- Pas de `path` spécifié devient `"path": "/example"`
- `"path": "example"` devient `"path": "/example"`
- `"path": "truc"` devient `"path": "/truc"`
- `"path": ""` devient `"path": "/"`
- `"path": "/example"` reste `"path": "/example"`
- `"path": "/truc"` reste `"path": "/truc"`
- `"path": "/"` reste `"path": "/"`

## Extensions

Il est possible de définir des extensions dans le dossier `extensions/<application>`.
Ces extensions doivent définir une fonction `run` et recevront en paramètres les valeurs spécifiées sous leur nom.

Voir [le dossier d'extensions pour YesWiki](./extensions/yeswiki) pour un exemple.
