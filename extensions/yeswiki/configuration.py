"""Amends the `wakka.config.php` configuration with the values passed"""
from os import path
from typing import Union

from ..utils import run_as_user


def run(application, params, dryrun):
    if dryrun:
        print(__name__, params)
    else:
        print("Adjusting configuration")
        final_path = application["settings"]["final_path"]
        ynh_app_id = application["settings"]["id"]
        with run_as_user(ynh_app_id):
            with open(path.join(final_path, "wakka.config.php"), "r+") as wakka_config:
                line = wakka_config.readline()
                place = 0
                while line:
                    if line[:2] == "?>":
                        wakka_config.seek(place)
                        wakka_config.truncate()
                        line = ""
                    else:
                        place = wakka_config.tell()
                        line = wakka_config.readline()

                for key, value in params.items():
                    key = php_escape(key)
                    value = as_php(value)
                    wakka_config.write(f"$wakkaConfig['{key}'] = {value};\n")


def php_escape(string: str, delimiter: str = "'") -> str:
    string = string.replace(delimiter, "\\" + delimiter)
    if delimiter == '"':
        string = string.replace("$", "\\$")
    return string


def as_php(variable: Union[str, list, tuple, dict, int, float, bool]) -> str:
    if isinstance(variable, str):
        return f"'{php_escape(variable)}'"
    elif isinstance(variable, bool):
        return "true" if variable else "false"
    elif isinstance(variable, int) or isinstance(variable, float):
        return str(variable)
    elif isinstance(variable, list) or isinstance(variable, tuple):
        parts = (as_php(item) for item in variable)
        return "[" + ", ".join(parts) + "]"
    elif isinstance(variable, dict):
        parts = (
            " => ".join((as_php(key), as_php(value))) for key, value in variable.items()
        )
        return "[" + ", ".join(parts) + "]"
