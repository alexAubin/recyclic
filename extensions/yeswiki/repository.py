"""Handles YesWiki repository downloads and validation"""
import subprocess
from enum import Enum
from enum import unique
from pathlib import Path

REPOSITORY_URL = "https://repository.yeswiki.net/"
VERSION_NAME = "doryphore"
FILE_FORMAT = "{type}-{name}-{version}.zip"


@unique
class Types(Enum):
    PLUGIN = "extension"
    THEME = "theme"


def download(yeswiki_path: Path, type: Types, name: str, version: str) -> bool:
    """Download, validate and extract a plugin or theme from the YesWiki repository"""
    filename = FILE_FORMAT.format(type=type.value, name=name, version=version)
    if type is Types.PLUGIN:
        filepath = yeswiki_path / "tools" / filename
    else:
        filepath = yeswiki_path / "themes" / filename

    remote = REPOSITORY_URL + VERSION_NAME + "/" + filename

    if _download(remote, filepath) and _validate(filepath):
        p = subprocess.run(
            ["unzip", str(filepath)],
            stdout=subprocess.PIPE,
            cwd=filepath.parent,
        )
        filepath.unlink()
        return p.returncode == 0
    else:
        print("Failed to download and validate " + type.value + " " + name)
        return False


def _validate(filepath: Path) -> bool:
    """Check a file with its checksum from the repository"""
    checksum_filepath = filepath.with_suffix(".zip.md5")
    checksum_filename = checksum_filepath.name
    if _download(
        REPOSITORY_URL + VERSION_NAME + "/" + checksum_filename,
        checksum_filepath,
    ):
        p = subprocess.run(
            ["md5sum", "-c", str(checksum_filepath)],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            cwd=checksum_filepath.parent,
        )
        checksum_filepath.unlink()
        return p.returncode == 0
    else:
        return False


def _download(origin: str, destination: Path) -> bool:
    """Download a file from `origin` to `destination`"""
    print("Downloading " + origin)
    p = subprocess.run(
        ["wget", "-O", destination, origin],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    return p.returncode == 0
